//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_pqAEVAShortcuts_h
#define smtk_session_aeva_pqAEVAShortcuts_h

#include "smtk/PublicPointerDefs.h"

#include <QToolBar>

class vtkSMSMTKWrapperProxy;
class pqSMTKWrapper;
class pqServer;

/**\brief Add global keyboard shortcuts to aeva.
  *
  */
class pqAEVAShortcuts : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  pqAEVAShortcuts(QObject* parent = nullptr);
  ~pqAEVAShortcuts() override;

  /// There should be only one toolbar. Return the singleton instance.
  static pqAEVAShortcuts* instance();

protected:
  class pqInternal;
  pqInternal* m_p;

private:
  Q_DISABLE_COPY(pqAEVAShortcuts);
};

#endif

<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "proximity feature" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="proximity feature" Label="Proximity Feature" BaseType="operation">

      <BriefDescription>Generate a surface selection by choosing cells proximal to a second surface.</BriefDescription>
      <DetailedDescription>
        Generate a surface selection by choosing cells within some given distance to a second surface.

        If no cells are within the specified distance, a message is logged to the console with the
        range of distances between the source and target objects.

        You may also enable thresholding that tests whether the direction of a candidate cell's
        normal is opposite the normal of the closest point on the target surface, to within the
        given angular deviation. Note that this option will usually produce poor results if the
        source and target surfaces intersect.
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <BriefDescription>The input data from which cells will be selected.</BriefDescription>
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Component Name="target" NumberOfRequiredValues="1">
          <BriefDescription>The second surface to which the input must be proximal.</BriefDescription>
          <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
        </Component>

        <Double Name="distance" Label="Distance" NumberOfRequiredValues="1">
          <BriefDescription>Cells with a majority of points within this distance will be selected.</BriefDescription>
          <DefaultValue>0.1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.</Min>
          </RangeInfo>
        </Double>

        <Double Name="angle" Label="Angle threshold" NumberOfRequiredValues="1" Units="degrees"
          Optional="true" IsEnabledByDefault="false">
          <BriefDescription>The maximum allowed angle between opposing surface normals.</BriefDescription>
          <DefaultValue>30.</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.</Min>
            <Min Inclusive="false">180.</Min>
          </RangeInfo>
        </Double>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(proximity feature)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>

<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "normal feature" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="normal feature" Label="Normal Feature" BaseType="operation">

      <BriefDescription>Generate a surface selection by choosing cells with normals in a given direction.</BriefDescription>
      <DetailedDescription>
        Generate a surface selection by choosing cells with normals in a direction.

        Note that the direction vector must not be zero-length.
        It is acceptable if it is not unit-length (it will be normalized as needed).
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1" Extensible="true">
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Double Name="direction" Label="Normal direction" NumberOfRequiredValues="3">
          <BriefDescription>Cells with normals in this direction will be added to the feature.</BriefDescription>
          <DefaultValue>0.0,0.0,1.0</DefaultValue>
        </Double>

        <Double Name="angle" Label="Angle threshold" Units="degrees" NumberOfRequiredValues="1">
          <BriefDescription>The maximum deviation from the normal direction that will be accepted.</BriefDescription>
          <DefaultValue>45.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.</Min>
            <Max Inclusive="true">360.</Max>
          </RangeInfo>
        </Double>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(normal feature)" BaseType="result">
      <ItemDefinitions>
        <Void Name="allow camera reset" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
